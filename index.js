class Animaster {
    _lastTranslation = { x: 0, y: 0 };
    _lastRatio = 1;
    _steps = [];
    _reset = [];
    constructor() {

    }

    /**
     * Блок плавно появляется из прозрачного.
     * @param {HTMLElement} element         — HTMLElement, который надо анимировать
     * @param {number}      duration        — Продолжительность анимации в миллисекундах
     * @returns {{reset:function}    — Объект с функцией сброса в исходное положение
     */
    fadeIn(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
        const animasterObject = this;
        return {
            reset: function () {
                animasterObject.resetFadeIn(element);
            }
        }
    }

    /**
     * Вовращает  блок в исходное положение, после  fadeIn
     * @param {HTMLElement} element     — HTMLElement, на котором нужно сбросить состояние
     */
    resetFadeIn(element) {
        element.style.transitionDuration = null;
        element.classList.remove('show');
        element.classList.add('hide');
    }

    /**
     * Блок плавно становится прозрачным.
     * @param {HTMLElement} element         — HTMLElement, который надо анимировать
     * @param {number}      duration        — Продолжительность анимации в миллисекундах\
     * @returns {{reset:function}}    — Объект с функцией сброса в исходное положение
     */
    fadeOut(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('show');
        element.classList.add('hide');
        const animasterObject = this;
        return {
            reset: function () {
                animasterObject.resetfadeOut(element);
            }
        }
    }

    /**
     * Вовращает  блок в исходное положение, после  fadeOut
     * @param {HTMLElement} element     — HTMLElement, на котором нужно сбросить состояние
     */
    resetfadeOut(element) {
        element.style.transitionDuration = null;
        element.classList.remove('hide');
        element.classList.add('show');
    }

    /**
     * Функция, передвигающая элемент
     * @param {HTMLElement}         element         — HTMLElement, который надо анимировать
     * @param {number}              duration        — Продолжительность анимации в миллисекундах
     * @param {{x:number,y:number}} translation     — объект с полями x и y, обозначающими смещение блока
     * @returns {{reset:function}}            — Объект с функцией сброса в исходное положение
     */
    move(element, duration, translation) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = this._getTransform(translation, this._lastRatio);
        this._lastTranslation = translation;
        const animasterObject = this;
        return {
            reset: function () {
                animasterObject.resetMoveAndScale(element);
            }
        }
    }

    /**
     * Вовращает  блок в исходное положение, после  move
     * @param {HTMLElement} element     — HTMLElement, на котором нужно сбросить состояние
     */
    resetMoveAndScale(element) {
        element.style.transitionDuration = null;
        element.style.transform = null;
    }

    /**
     * Функция, увеличивающая/уменьшающая элемент
     * @param {HTMLElement} element         — HTMLElement, который надо анимировать
     * @param {number}      duration        — Продолжительность анимации в миллисекундах
     * @param {number}      ratio           — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
     * @returns {{reset:function}}    — Объект с функцией сброса в исходное положение
     */
    scale(element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = this._getTransform(this._lastTranslation, ratio);
        this._lastRatio = ratio;
        const animasterObject = this;
        return {
            reset: function () {
                animasterObject.resetMoveAndScale(element);
            }
        }
    }
    /**
     * Функция, передвигающая элемент, который потом исчезает
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     * @param {number}      duration    — Продолжительность анимации в миллисекундах
     */
    moveAndHide(element, duration) {
        return this.addMove(duration * 0.4, { x: 100, y: 20 }).addFadeOut(duration * 0.6).play(element);
    }

    /**
     * Блок появляется,ждет и исчезает. Каждый шаг анимации длится 1/3 от времени, переданного аргументом в функцию.
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     * @param {number}      duration    — Продолжительность анимации в миллисекундах
     */
    showAndHide(element, duration) {
        return this
            .addFadeIn(duration / 3)
            .addDelay(duration / 3)
            .addFadeOut(duration / 3)
            .play(element);
    }

    /**
     * Имитация сердцебиения. Сначала элемент увеличится в 1.4 раза, потом обратно к 1. Каждый шаг анимации занимает 0.5 секунды.
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     */
    heartBeating(element) {
        return this.addScale(500, 1.4).addScale(500, 1).play(element, true);
    }

    /**
     * Дрожание элемента. Элемент двигается слева на право на 20px и возвращается обратно. Длительность анимации - 0.5 сек. 
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     */
    shaking(element) {
        return this.addMove(250, { x: 20, y: 0 }).addMove(250, { x: 0, y: 0 }).play(element, true);
    }


    /**
     * Помешает анимацию move в очередь исполнения анимаций
     * @param {number}              duration        — Продолжительность анимации в миллисекундах
     * @param {{x:number,y:number}} translation     — объект с полями x и y, обозначающими смещение блока
     */
    addMove(duration, translation) {
        this._steps.push({
            name: 'move',
            duration: duration,
            translation: translation
        })
        return this;
    }
    /** 
     * делает ничего, в течение заданого времени
     * @param {HTMLElement}         element         — HTMLElement, который надо анимировать
     * @param {number}              duration        — Продолжительность анимации в миллисекундах 
    */
    _empty(element, duration) {
        //тут ничего не происходит
        return { reset: this._empty }
    }

    /**
     * Помещает задержку в очередь исполнения анимаций
     * @param {number} duration  — Продолжительность анимации в миллисекундах 
     */
    addDelay(duration) {
        this._steps.push({
            name: '_empty',
            duration: duration,
        })
        return this;
    }

    /**
     * Помещает анимацию scale в очередь исполнения анимаций
     * @param {number} duration      — Продолжительность анимации в миллисекундах 
     * @param {number} ratio         — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
     */
    addScale(duration, ratio) {
        this._steps.push({
            name: 'scale',
            duration: duration,
            ratio: ratio
        })
        return this;
    }

    /**
     * Помещает анимацию fadeIn в очередь исполнения анимаций
     * @param {number} duration  — Продолжительность анимации в миллисекундах 
     */
    addFadeIn(duration) {
        this._steps.push({
            name: 'fadeIn',
            duration: duration,
        })
        return this;
    }

    /**
     * Помещает анимацию fadeOut в очередь исполнения анимаций
     * @param {number} duration 
     */
    addFadeOut(duration) {
        this._steps.push({
            name: 'fadeOut',
            duration: duration,
        })
        return this;
    }
    /**
     * Очищает очередь исполнения анимаций
     */
    clear() {
        this._steps.splice(0);
    }
    /**
     * Выполняет созданный очередь анимаций
     * @param {HTMLElement}                          element         — HTMLElement, который надо анимировать
     * @param {boolean}                              cycled          — флаг зацикленности анимации
     * @param {boolean}                              resetEnd        — флаг автоматического сброса в исходное положение, после выполнения анимации
     * @returns {{stop:function, reset:function}}                    — Объект с методами остановки и сброса анимации
     * stop — функция остановки циклических анимаций,отсутсвует, если cycled=false
     * reset — функция сброса анимаций 
     */
    play(element, cycled, resetEnd) {
        let animationHandle;
        const animasterObject = this;

        function startAnimation(index, cycled, animasterObject) {
            const { duration, name, translation, ratio } = { ...animasterObject._steps[index] };
            animationHandle = setTimeout(() => {
                animasterObject._reset.unshift(animasterObject[name](element, duration, translation ? translation : ratio));
                if (index + 1 < animasterObject._steps.length) {
                    startAnimation(index + 1, cycled, animasterObject)
                } else if (cycled) {
                    startAnimation(0, cycled, animasterObject);
                }
            }, duration)
        }

        startAnimation(0, cycled, animasterObject);

        function reset(duration) {
            setTimeout(() => {
                stop();
                animasterObject._reset.forEach((resetAnimation) => {
                    resetAnimation.reset();
                })
                animasterObject._reset = [];
            }, duration ? duration : 0)
        }

        function stop() {
            clearInterval(animationHandle);
        }

        const returnObj = { reset: reset };

        if (resetEnd) {
            const sumDuration = this._steps.reduce((a, b) => a + b.duration, 500);
            reset(sumDuration);
        }
        if (cycled) {
            returnObj.stop = stop;
        }

        return returnObj;
    }

    /**
     * Создает обработчик события для воиспроизведения анимации
     * @param {boolean} cycled       — флаг зацикленности анимации
     * @param {boolean} resetEnd     — флаг автоматического сброса в исходное положение, после выполнения анимаций
     * @returns {Function} 
     */
    buildHandler(cycled, resetEnd) {
        const animasterObject = this;
        return function () {
            animasterObject.play(this, cycled, resetEnd);
        }
    }

    _getTransform(translation, ratio) {
        const result = [];
        if (translation) {
            result.push(`translate(${translation.x}px,${translation.y}px)`);
        }
        if (ratio) {
            result.push(`scale(${ratio})`);
        }
        return result.join(' ');
    }
}
(function main() {
    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            const { reset } = new Animaster().fadeIn(block, 5000);
            document.getElementById('fadeInReset').addEventListener('click', function () {
                reset();
            });
        });

    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');
            const { reset } = new Animaster().move(block, 1000, { x: 100, y: 10 });
            document.getElementById('moveReset').addEventListener('click', function () {
                reset();
            });
        });

    document.getElementById('scalePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('scaleBlock');
            const { reset } = new Animaster().scale(block, 1000, 1.25);
            document.getElementById('scaleReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('fadeOutPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeOutBlock');
            const { reset } = new Animaster().fadeOut(block, 1000, 1.25);
            document.getElementById('fadeOutReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveAndHideBlock');
            const { reset } = new Animaster().moveAndHide(block, 1000, 1.25);
            document.getElementById('moveAndHideReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('showAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('showAndHideBlock');
            const { reset } = new Animaster().showAndHide(block, 1000, 1.25);
            document.getElementById('showAndHideReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('heartBeatingBlock');
            const { stop, reset } = new Animaster().heartBeating(block, 1000, 1.25);
            document.getElementById('heartBeatingStop').addEventListener('click', function () {
                stop();
            });
            document.getElementById('heartBeatingReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('shakingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('shakingBlock');
            const { stop, reset } = new Animaster().shaking(block, 1000, 1.25);
            document.getElementById('shakingStop').addEventListener('click', function () {
                stop();
            });
            document.getElementById('shakingReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('customPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('customBlock');
            const customAnimation = new Animaster()
                .addMove(1000, { x: 100, y: 10 })
                .addMove(1000, { x: 50, y: 0 })
                .addScale(500, 1.25)
                .addFadeOut(500)
                .addMove(500, { x: 100, y: 10 })
                .addFadeIn(500)
                .addScale(500, 1)
                .addMove(500, { x: 0, y: 0 });
            const { reset, stop } = customAnimation.play(block, true);
            document.getElementById('customReset').addEventListener('click', function () {
                reset();
            });
            document.getElementById('customStop').addEventListener('click', function () {
                stop();
            });
        });
    document.getElementById('customHandlerBlock')
        .addEventListener('click',
            new Animaster()
                .addMove(1000, { x: 100, y: 10 })
                .addMove(1000, { x: 50, y: 0 })
                .addScale(500, 1.25)
                .addFadeOut(500)
                .addMove(500, { x: 100, y: 10 })
                .addFadeIn(500).buildHandler(false, true)
        );
})();

